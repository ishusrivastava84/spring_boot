package com.gl.jdbc.movie.entity;

import java.time.LocalDate;

public class Booking {

    private Long bookingId;
    private LocalDate bookingDate;
    private int seatNo;
    private double amount;
    private Long customerId;
    private String movieId;

    public Booking(){
        //No args cons
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        bookingId = bookingId;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(int seatNo) {
        this.seatNo = seatNo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        movieId = movieId;
    }

    public Booking(Long bookingId, LocalDate bookingDate, int seatNo, double amount, Long customerId, String movieId) {
        bookingId = bookingId;
        this.bookingDate = bookingDate;
        this.seatNo = seatNo;
        this.amount = amount;
        this.customerId = customerId;
        movieId = movieId;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "BookingId=" + bookingId +
                ", bookingDate=" + bookingDate +
                ", seatNo=" + seatNo +
                ", amount=" + amount +
                ", customerId=" + customerId +
                ", MovieId='" + movieId + '\'' +
                '}';
    }
}
