package com.gl.jdbc.movie.entity;

public class Movie {

    private String movieId;
    private String movieName;
    private String language;
    private int releaseIn;
    private long revenueInDoller;

    public Movie(){
        //No args cons
    }

    public String getMovieId() {
        return movieId;
    }


    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;

    }

    public int getReleaseIn() {
        return releaseIn;
    }

    public int setReleaseIn(int releaseIn) {
        this.releaseIn = releaseIn;
        return releaseIn;
    }

    public long getRevenueInDoller() {
        return revenueInDoller;
    }

    public void setRevenueInDoller(long revenueInDoller) {
        this.revenueInDoller = revenueInDoller;
    }

    public Movie(String movieId, String movieName, String language, int releaseIn, long revenueInDoller) {
        this.movieId = movieId;
        this.movieName = movieName;
        this.language = language;
        this.releaseIn = releaseIn;
        this.revenueInDoller = revenueInDoller;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieId='" + movieId + '\'' +
                ", movieName='" + movieName + '\'' +
                ", language='" + language + '\'' +
                ", releaseIn=" + releaseIn +
                ", revenueInDoller=" + revenueInDoller +
                '}';
    }



}
