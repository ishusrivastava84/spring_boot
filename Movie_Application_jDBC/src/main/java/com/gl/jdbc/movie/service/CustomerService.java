package com.gl.jdbc.movie.service;

import com.gl.jdbc.movie.db.MovieDB;
import com.gl.jdbc.movie.entity.Customer;

import java.sql.SQLException;

public class CustomerService {
    Customer c=new Customer();
    public void addCustomer() throws SQLException {
        new MovieDB().addCustomer(c);
    }

    public void deleteCustomer() throws SQLException {
        new MovieDB().deleteCustomer(c);
    }

    public void updateCustomer() throws SQLException {
        new MovieDB().updateCustomer();
    }

}
