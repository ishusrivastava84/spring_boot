package com.gl.jdbc.movie.service;

import com.gl.jdbc.movie.db.MovieDB;
import com.gl.jdbc.movie.entity.Movie;

import java.sql.SQLException;

public class MovieService {

    Movie m=new Movie();
    public void addMovie(Movie movie) throws SQLException{
        System.out.println("Inside Add Movie");

        try {
            new MovieDB().addMovie(m); //Database
        }
        catch (Exception s){
            System.out.println("DB Error");
        }

    }

    public void getMovieByYear() throws SQLException{
        System.out.println("Inside getMovieByYear");

        try {
            new MovieDB().getMovie(m);
        }
        catch (Exception e) {
            System.out.println("DB Error");
        }

    }

    public void updateRevenue() throws SQLException{
        System.out.println("Inside update Revenue");

        try {
            new MovieDB().updateRevenue(m);
        }
        catch (Exception e){
            System.out.println("DB Erro");
        }
    }

    public void deleteMovieById() throws SQLException{
        System.out.println("Inside Delete Movie");

        try {
            new MovieDB().deleteMovie(m);
        }
        catch (Exception e){
            System.out.println("DB Error");

        }

    }
}
