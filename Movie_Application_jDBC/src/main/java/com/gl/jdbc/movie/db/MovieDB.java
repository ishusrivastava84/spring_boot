package com.gl.jdbc.movie.db;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import com.gl.jdbc.movie.entity.Booking;
import com.gl.jdbc.movie.entity.Customer;
import com.gl.jdbc.movie.entity.Movie;

public class MovieDB {

    private final String url = "jdbc:postgresql://localhost:5432/movie_db";
    private final String username = "postgres";
    private final String password = "ishu123";

    private final Connection conn = DriverManager.getConnection(this.url, this.username, this.password);

    private Statement stmt = conn.createStatement();

    public MovieDB() throws SQLException {
    }

    Scanner sc=new Scanner(System.in);
    public void addMovie(Movie m) throws SQLException {
        StringBuilder string1 = new StringBuilder("INSERT INTO movie VALUES('M1001', 'Home Alone', 'English', 1990, 476700000);");
        stmt.execute(string1.toString());
        StringBuilder string2 = new StringBuilder("INSERT INTO movie VALUES('M1002', 'Home Alone', 'English', 1992, 358900000);");
        stmt.execute(string2.toString());
        StringBuilder string3 = new StringBuilder("INSERT INTO movie VALUES('M1003','Matrix','English',1994, 16800000);");
        stmt.execute(string3.toString());

//       string.append("Values ('").append(m.setMovieId(sc.nextLine())).append("','").append(m.setMovieName(sc.nextLine())).append("','").append(m.setLanguage(sc.nextLine())).append("',")
//                .append(m.setReleaseIn(sc.nextInt())).append(",").append(m.setRevenueInDoller(sc.nextLong())).append(")");
//        System.out.println("Data Added");


            conn.close();
    }

    public void getMovie(Movie m) throws SQLException {
        System.out.print("Please enter the release date =");
        int year= sc.nextInt();
        ArrayList<Movie> movie = new  ArrayList<Movie>();
        StringBuilder string = new StringBuilder("select * from movie where releasedin >");
        string.append(year).append(";");
        ResultSet rs=stmt.executeQuery(string.toString());

        while (rs.next()) {
            m.setMovieId(rs.getString("movieid"));
            m.setMovieName(rs.getString("moviename"));
            m.setLanguage(rs.getString("language"));
            m.setReleaseIn(rs.getInt("releasedin"));
            m.setRevenueInDoller(rs.getLong("revenueindollars "));
            movie.add(m);
            System.out.println(m.getMovieId()+":"+m.getMovieName()+":"+m.getLanguage()+":"+m.getReleaseIn()+":"+m.getRevenueInDoller());
        }
        rs.close();
        conn.close();

    }


    public void updateRevenue(Movie m) throws SQLException {
        System.out.println("Inside update revenue");
        System.out.print("Please Enter the movieId: ");
        String movieId=sc.nextLine();
        System.out.print("Please Enter the updated revenue: ");
        long revenue= sc.nextLong();

        StringBuilder string = new StringBuilder("update movie set revenueindollars ="+revenue+ "where movieid ='" +movieId+"'");
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("Data updated Succesfully");
    }

    public void deleteMovie(Movie m) throws SQLException {
        System.out.print("Plase enter the movie id which you want to delete: ");
        String movieId=sc.nextLine();
        StringBuilder string = new StringBuilder("delete from movie where movieid ='"+movieId+"'");
        stmt.execute(string.toString());
        conn.close();
        System.out.println("Data deletd Successfully");
    }

    public void addCustomer(Customer c) throws SQLException {
        System.out.println("Inside add customer");
        StringBuilder string = new StringBuilder("inert into customer(customer_id,customer_name,customer_age,customer_address)")
                .append("Values ('").append("1,'ishu,24,'kanpur'").append(")");
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("Data Inserted Properly");
    }

    public void deleteCustomer(Customer c) throws SQLException {
        System.out.println("Inside delete customer");
        System.out.println("Enter the customer id which you want to delete");
        long id=sc.nextLong();
        StringBuilder string = new StringBuilder("delete from customer where customer_id = ").append(id);
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("customer delete successfully");
    }

    public void updateCustomer() throws SQLException {
        System.out.println("Inside update Customer");
        System.out.println("Enter the customer id which you want to update");
        long id=sc.nextLong();
        System.out.println("Enter the new customer name which you want to update");
        String name=sc.nextLine();
        System.out.println("Enter the new customer age which you want to update");
        int age=sc.nextInt();
        System.out.println("Enter the new customer address which you want update");
        String address=sc.nextLine();

        StringBuilder string = new StringBuilder("update customer set customer_name ='")
                .append(name).append("',").append("customer_age =").append(age).append(",").append("customer_address ='").append(address).append("'")
                .append("where customer_id =").append(id);
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("data updated successfully");
    }

    public void bookMovie(Booking b) throws SQLException {
        System.out.println("Inside Book Movie");
        System.out.println("Enter the customer_id");
        long cus_id=sc.nextLong();
        System.out.println("Enter the movie_id");
        String mov_id=sc.nextLine();
        StringBuilder string = new StringBuilder("insert into bookmovie(customer_id,movie_id) Values (")
                .append("customer_id = ").append(cus_id).append(", movieid ='").append(mov_id).append("'");
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("your movie booking is done your booking id = " + b.getBookingId());

    }

    public void deleteBooking(Booking b) throws SQLException {
        System.out.println("Inside Delete Booking");
        System.out.println("Enter the booking id which you want to delete ");
        long id=sc.nextLong();
        StringBuilder string = new StringBuilder("delete from booking where booking_id = ")
                .append(id);
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("booking is deleted");
    }

    public void updateBooking(Booking b) throws SQLException {
        System.out.println("Inside update booking");
        System.out.println("Enter the Booking Id which you want to update ");
        long book_id=sc.nextLong();
        System.out.println("Enter the Customer Id which you want to update ");
        long cus_id=sc.nextLong();

        StringBuilder string = new StringBuilder("update booking set booking_id = ")
                .append(book_id).append(",").append("customer_id =").append(cus_id);
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("Booking Updated Successfully");
    }

    public void getAllBooking(Booking b) throws SQLException {
        System.out.println("Inside Get All Booking");
        StringBuilder string = new StringBuilder("select * from booking");
        ResultSet rs =stmt.executeQuery(string.toString());
        LinkedList<String> ls=new LinkedList<>();
        ls.add(rs.toString());
        }

    }

