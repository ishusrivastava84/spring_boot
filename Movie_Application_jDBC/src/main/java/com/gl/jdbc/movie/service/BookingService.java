package com.gl.jdbc.movie.service;

import com.gl.jdbc.movie.db.MovieDB;
import com.gl.jdbc.movie.entity.Booking;

import java.sql.SQLException;

public class BookingService {

    Booking b = new Booking();
    public void bookMovie() throws SQLException {
        new MovieDB().bookMovie(b);
    }

    public void deleteBooking() throws SQLException {
        new MovieDB().deleteBooking(b);
    }

    public void updateBooking() throws SQLException {
        new MovieDB().updateBooking(b);
    }

    public void getAllBooking() throws SQLException {
        new MovieDB().getAllBooking(b);
    }

}
