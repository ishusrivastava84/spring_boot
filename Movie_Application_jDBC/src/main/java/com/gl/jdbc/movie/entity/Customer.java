package com.gl.jdbc.movie.entity;

public class Customer {

    private Long customerId;
    private String customerName;
    private int age;
    private String address;

    public Customer(){
        //No args
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Customer(Long customerId, String customerName, int age, String address) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.age = age;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", customerName='" + customerName + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
