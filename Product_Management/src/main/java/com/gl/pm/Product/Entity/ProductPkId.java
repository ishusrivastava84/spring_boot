package com.gl.pm.Product.Entity;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
public class ProductPkId implements Serializable {
    private Long productCode;
    private String productName;

    public ProductPkId(){

    }
    public Long getProductCode() {
        return productCode;
    }

    public void setProductCode(Long productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public ProductPkId(Long productCode, String productName) {
        this.productCode = productCode;
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "ProductPkId{" +
                "productCode=" + productCode +
                ", productName='" + productName + '\'' +
                '}';
    }
}
