package com.gl.pm.Product.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(ProductPkId.class)
public class Product{

    @Id
    private Long productCode;
    @Id
    private String productName;
    private String Spacification;
    private String ProductReview;
    private double productCost;

    public Product(){

    }

    public Long getProductCode() {
        return productCode;
    }

    public void setProductCode(Long productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSpacification() {
        return Spacification;
    }

    public void setSpacification(String spacification) {
        Spacification = spacification;
    }

    public String getProductReview() {
        return ProductReview;
    }

    public void setProductReview(String productReview) {
        ProductReview = productReview;
    }

    public double getProductCost() {
        return productCost;
    }

    public void setProductCost(double productCost) {
        this.productCost = productCost;
    }

    public Product(Long productCode, String productName, String spacification, String productReview, double productCost) {
        this.productCode = productCode;
        this.productName = productName;
        Spacification = spacification;
        ProductReview = productReview;
        this.productCost = productCost;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productCode=" + productCode +
                ", productName='" + productName + '\'' +
                ", Spacification='" + Spacification + '\'' +
                ", ProductReview='" + ProductReview + '\'' +
                ", productCost=" + productCost +
                '}';
    }
}
