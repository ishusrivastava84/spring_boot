package com.gl.pm.Product.Services;

import com.gl.pm.Product.Entity.Category;
import com.gl.pm.Product.Entity.Product;

import java.util.List;

public interface CtaProService {
    Product addProduct(Product product);

    Category addCategory(Category category);

    Product findProductById(Long productCode);

    Category findCategoryById(Long categoryCode);

    List<Product> findAllProducts();

    List<Category> findAllCategory();

    Product findProductByName(String productName);

    Category findCategoryByType(String categoryType);

    Product updateProductById(Long productCode, Product product);

    void deleteProductById(Long productCode);

    //void deleteProductById(Long productCode);
}
