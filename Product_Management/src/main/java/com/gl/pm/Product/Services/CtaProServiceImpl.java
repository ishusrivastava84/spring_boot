package com.gl.pm.Product.Services;

import com.gl.pm.Product.Entity.Category;
import com.gl.pm.Product.Entity.Product;
import com.gl.pm.Product.Repository.CategoryRepo;
import com.gl.pm.Product.Repository.CategoryRepo2;
import com.gl.pm.Product.Repository.ProductRepo;
import com.gl.pm.Product.Repository.ProductRepo2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CtaProServiceImpl implements CtaProService{

    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private CategoryRepo categoryRepo;
    @Autowired
    private ProductRepo2 productRepo2;
    @Autowired
    private CategoryRepo2 categoryRepo2;

    @Override
    public Product addProduct(Product product) {
        return productRepo.save(product);
    }

    @Override
    public Category addCategory(Category category) {
        return categoryRepo.save(category);
    }

    @Override
    public Product findProductById(Long productCode) {
        return productRepo.findById(productCode).get();
    }

    @Override
    public Category findCategoryById(Long categoryCode) {
        return  categoryRepo.findById(categoryCode).get();
    }

    @Override
    public List<Product> findAllProducts() {
        return productRepo.findAll();
    }

    @Override
    public List<Category> findAllCategory() {
        return categoryRepo.findAll();
    }

    @Override
    public Product findProductByName(String productName) {
        return productRepo2.findById(productName).get();
    }

    @Override
    public Category findCategoryByType(String categoryType) {

        return categoryRepo2.findById(categoryType).get();
    }

    @Override
    public Product updateProductById(Long productCode, Product product) {
        Product pro= productRepo.findById(productCode).get();
        return productRepo.save(product);
    }

    @Override
    public void deleteProductById(Long productCode) {
        productRepo.deleteById(productCode);
    }

//    @Override
//    public void deleteProductById(Long productCode) {
//        productRepo.deleteById(productCode);
//    }


}
