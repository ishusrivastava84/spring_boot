package com.gl.contacts.Entity;

import java.util.List;


public class Contacts {
    private String name;
    private String organization;
    private List<PhoneNumber> phoneNumberList;
    private List<Address> addressList;

    public Contacts(){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public List<PhoneNumber> getPhoneNumberList() {
        return phoneNumberList;
    }

    public void setPhoneNumberList(List<PhoneNumber> phoneNumberList) {
        this.phoneNumberList = phoneNumberList;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public Contacts(String name, String organization, List<PhoneNumber> phoneNumberList, List<Address> addressList) {
        this.name = name;
        this.organization = organization;
        this.phoneNumberList = phoneNumberList;
        this.addressList = addressList;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "name='" + name + '\'' +
                ", organization='" + organization + '\'' +
                ", phoneNumberList=" + phoneNumberList +
                ", addressList=" + addressList +
                '}';
    }
}
