package com.gl.contacts;

import com.gl.contacts.Entity.Address;
import com.gl.contacts.Entity.Contacts;
import com.gl.contacts.Entity.PhoneNumber;

import java.sql.SQLException;

public class AddressBook {

    Contacts cs=new Contacts();
    PhoneNumber pn=new PhoneNumber();
    Address ad=new Address();

    public void addcontact() throws SQLException {
        new DBclass().addContact(cs,pn,ad);
    }

    public void searchByName() throws SQLException {
        new DBclass().searchByName(cs,pn,ad);
    }

    public void searchByOrganization() throws SQLException {
        new DBclass().searchByOrganization(cs,pn,ad);
    }

    public void updateContact() throws SQLException {
        new DBclass().updateContact(cs,pn,ad);
    }

    public void deleteContact() throws SQLException {
        new DBclass().deleteContact(cs,pn,ad);
    }

}
