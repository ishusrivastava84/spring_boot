package com.gl.contacts;

import com.gl.contacts.Entity.Address;
import com.gl.contacts.Entity.Contacts;
import com.gl.contacts.Entity.PhoneNumber;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DBclass {

    private final String url = "jdbc:postgresql://localhost:5432/postgres";
    private final String username = "postgres";
    private final String password = "ishu123";

    private final Connection conn = DriverManager.getConnection(this.url, this.username, this.password);
    private Statement stmt = conn.createStatement();
    List<PhoneNumber> ls=new ArrayList<PhoneNumber>();
    List<Address> address=new ArrayList<Address>();
    List<Contacts> contacts =new ArrayList<Contacts>();
    Scanner sc=new Scanner(System.in);

    public DBclass() throws SQLException {
    }

    public void addContact(Contacts cs, PhoneNumber pn, Address ad) throws SQLException {
        pn.setLabel("Ishu");
        pn.setPhoneNumber("9919787824");
        ls.add(pn);

        ad.setAddress("Orai");
        ad.setLabel("home");
        address.add(ad);

        contacts.add(new Contacts("Ishu","IT",ls,address));
        System.out.println(contacts);

        StringBuilder string = new StringBuilder("insert into contacts(contact_name,contact_org,contact_phonenumber,contact_address)")
                .append("Values ('ishu'").append(",'IT").append("',").append("'{ishu,9919787824}'")
                .append(",'").append("{orai,home}'").append(")");
        stmt.execute(string.toString());
        conn.close();

    }
    public void searchByName(Contacts cs, PhoneNumber pn, Address ad) throws SQLException {
        System.out.println("Please enter the name of contact");
        String name=sc.nextLine();
        StringBuilder string=new StringBuilder("select * from contacts where contact_name = '").append(name).append("'");
        ResultSet rs=stmt.executeQuery(string.toString());

        while(rs.next()){
            String con_name=rs.getString("contact_name");
            String con_org=rs.getString("contact_org");
            String str=rs.getString("contact_phonenumber");
            String str1=rs.getString("contact_address");

            String output=("Contact %s - %s - %s - %s");
            System.out.println(String.format(output,con_name,con_org,str,str1));

        }
        conn.close();
    }

    public void searchByOrganization(Contacts cs, PhoneNumber pn, Address ad) throws SQLException {
        System.out.println("Please enter the Organization");
        String name=sc.nextLine();
        StringBuilder string=new StringBuilder("select * from contacts where contact_org = '").append(name).append("'");
        ResultSet rs=stmt.executeQuery(string.toString());

        while(rs.next()) {
            String con_name = rs.getString("contact_name");
            String con_org = rs.getString("contact_org");
            String str = rs.getString("contact_phonenumber");
            String str1 = rs.getString("contact_address");

            String output = ("Contact %s - %s - %s - %s");
            System.out.println(String.format(output, con_name, con_org, str, str1));
            }
             conn.close();
        }


    public void updateContact(Contacts cs, PhoneNumber pn, Address ad) throws SQLException {
        System.out.println("Please enter the name of contact which you want to update ");
        String name=sc.nextLine();
        System.out.println("Please enter the name you want set ");
        String newname=sc.nextLine();
        System.out.println("Please enter the org you want to update ");
        String org=sc.nextLine();
        System.out.println("please enter the phone number to update ");
        String phone=sc.nextLine();
        System.out.println("please enter the address to update ");
        String address=sc.nextLine();

        StringBuilder string=new StringBuilder("update contacts set contact_name ='").append(newname).append("',contact_org ='").append(org)
                .append("',contact_phonenumber ='").append(phone).append("',contact_address ='").append(address).append("' where contact_name ='").append(name).append("'");

        stmt.executeQuery(string.toString());
        System.out.println("Data updated Successfully");
        conn.close();

        }

    public void deleteContact(Contacts cs, PhoneNumber pn, Address ad) throws SQLException {
        System.out.println("Please enter the name of contact which you want to delete ");
        String name=sc.nextLine();
        StringBuilder string=new StringBuilder("delete from contacts where contact_name ='").append(name).append("'");
        System.out.println("Contact deleted sucesfully");
        conn.close();
    }
}

