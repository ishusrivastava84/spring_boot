package com.gl.game;

import com.gl.game.DB.GameDB;

import java.io.IOException;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, IOException {

        System.out.println("Gamming Application");

        GameDB db=new GameDB();
       // db.InsertGame();
        db.DisplayGame();
    }
}