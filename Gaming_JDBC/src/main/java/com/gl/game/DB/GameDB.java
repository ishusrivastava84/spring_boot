package com.gl.game.DB;

import com.gl.game.Entity.Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameDB {

    private final String url = "jdbc:postgresql://localhost:5432/game_db";
    private final String username = "postgres";
    private final String password = "ishu123";
    private final Connection conn = DriverManager.getConnection(this.url, this.username, this.password);
    private Statement stmt = conn.createStatement();
    Game game=new Game();
    Scanner sc=new Scanner(System.in);
    static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    public GameDB() throws SQLException {
    }

    public void InsertGame() throws SQLException, IOException {
        System.out.println("Inside InsertGame");
        System.out.println("Input Game Id: ");
        //int id= sc.nextInt();
        Integer id=Integer.parseInt(br.readLine());
        game.setGameId(id);

        System.out.println("Input Game Name: ");
        //String name=sc.nextLine();
        String name=br.readLine();
        game.setGameName(name);


        System.out.println("Input No of player");
        //int noofplayer=sc.nextInt();
        Integer noofplayer=Integer.parseInt(br.readLine());
        game.setNoOfPlayer(noofplayer);


        StringBuilder string=new StringBuilder("Insert into game(gameid,gamename,noofplayer)");
        string.append("Values (").append(game.getGameId()).append(",'").append(game.getGameName()).append("',").append(game.getNoOfPlayer()).append(")");
        stmt.execute(string.toString());
        conn.close();
        System.out.println("Data is inserted");
    }
    public void UpdateGame() throws SQLException, IOException {
        System.out.println("Inside UpdateGame");
        System.out.println("Input Game Id: ");
        //int id= sc.nextInt();
        Integer id=Integer.parseInt(br.readLine());
        game.setGameId(id);

        System.out.println("Input Game Name: ");
        //String name=sc.nextLine();
        String name=br.readLine();
        game.setGameName(name);


        System.out.println("Input No of player");
        //int noofplayer=sc.nextInt();
        Integer noofplayer=Integer.parseInt(br.readLine());
        game.setNoOfPlayer(noofplayer);

        StringBuilder string=new StringBuilder("Update game SET gamename=");
        string.append("'").append(game.getGameName()).append("',").append("noofplayer=").append(game.getNoOfPlayer()).append("WHERE gameid ").append(game.getGameId());
        stmt.execute(string.toString());
        conn.close();
    }
    public void DeleteGame() throws SQLException, IOException {
        System.out.println("Inside DeleteGame");
        System.out.println("Input Game Id: ");
        //int id= sc.nextInt();
        Integer id=Integer.parseInt(br.readLine());
        game.setGameId(id);

        StringBuilder string = new StringBuilder("Delete from game where gameid ").append(game.getGameId());
        stmt.execute(string.toString());
        conn.close();
    }
    public void DisplayGame() throws SQLException {
        StringBuilder string =new StringBuilder("select * from game");
        stmt.execute(string.toString());
        ResultSet rs=stmt.executeQuery(string.toString());
        while(rs.next()){
            List<Game> display = new ArrayList<Game>();
            System.out.println(display);
        }

        conn.close();

    }



}
