package com.gl.user.service;

import com.gl.user.VO.Department;
import com.gl.user.VO.ResponseTemplateVO;
import com.gl.user.entity.User;
import com.gl.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserSeriveImpl implements UserService{

    @Autowired
    private UserRepository usrRepo;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public User createUser(User user) {
        return usrRepo.save(user);
    }

    @Override
    public ResponseTemplateVO getUserWithDepartment(Long userId) {
        ResponseTemplateVO vo=new ResponseTemplateVO();
        User user=usrRepo.findByUserId(userId);

        Department department = restTemplate.getForObject("http://localhost:9090/Dep/"+user.getDepartmentId()+"/Get",Department.class);

        vo.setUser(user);
        vo.setDepartment(department);

        return vo;
    }


}
