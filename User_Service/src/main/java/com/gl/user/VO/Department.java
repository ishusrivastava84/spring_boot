package com.gl.user.VO;

public class Department {

    private Long departemntId;
    private String departmentName;

    private String departmentAddress;

    private String departmentCode;

    public Department(Long departemntId, String departmentName, String departmentAddress, String departmentCode) {
        this.departemntId = departemntId;
        this.departmentName = departmentName;
        this.departmentAddress = departmentAddress;
        this.departmentCode = departmentCode;
    }

    public Department(){

    }

    public Long getDepartemntId() {
        return departemntId;
    }

    public void setDepartemntId(Long departemntId) {
        this.departemntId = departemntId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentAddress() {
        return departmentAddress;
    }

    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departemntId=" + departemntId +
                ", departmentName='" + departmentName + '\'' +
                ", departmentAddress='" + departmentAddress + '\'' +
                ", departmentCode='" + departmentCode + '\'' +
                '}';
    }
}
