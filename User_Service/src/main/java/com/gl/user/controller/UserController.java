package com.gl.user.controller;

import com.gl.user.VO.ResponseTemplateVO;
import com.gl.user.entity.User;
import com.gl.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService usrser;

    @PostMapping("/usr/crt")
    public User createUser(@RequestBody User user){
        return usrser.createUser(user);
    }

    @GetMapping("/{id}")
    public ResponseTemplateVO getUserWithDepartment(@PathVariable("id") Long userId){
        return usrser.getUserWithDepartment(userId);
    }
}
