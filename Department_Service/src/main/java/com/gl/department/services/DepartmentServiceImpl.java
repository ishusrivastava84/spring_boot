package com.gl.department.services;

import com.gl.department.controller.DepartmentController;
import com.gl.department.entity.Department;
import com.gl.department.error.DepartmentException;
import com.gl.department.repository.DepartmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentServcie {

    @Autowired
    private DepartmentRepo depRepo;

    @Override
    public Department createDepartment(Department department) {
        return depRepo.save(department);
    }

    @Override
    public Department findDepartmentById(Long departmentId) throws DepartmentException {
        Optional<Department> department= depRepo.findById(departmentId);

        if(!department.isPresent()){
            throw new DepartmentException("Department Not Found");
        }
        return department.get();

    }

    @Override
    public void deleteById(Long departmentId) {
        depRepo.deleteById(departmentId);
    }

    @Override
    public Department updateById(Long departmentId, Department department) {
        Department dep=new Department();
        dep.setDepartemntId(departmentId);
        dep.setDepartmentName(department.getDepartmentName());
        dep.setDepartmentAddress(department.getDepartmentAddress());
        dep.setDepartmentCode(department.getDepartmentCode());
        return depRepo.save(dep);
    }

    @Override
    public List<Department> GetAllDepartment() {
        return depRepo.findAll();
    }


}
