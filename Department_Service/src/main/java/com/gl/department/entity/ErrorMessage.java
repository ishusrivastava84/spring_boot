package com.gl.department.entity;

import org.apache.http.HttpStatus;

public class ErrorMessage {

    private HttpStatus status;
    private String messgae;

    public ErrorMessage(HttpStatus status, String messgae) {
        this.status = status;
        this.messgae = messgae;
    }

    public ErrorMessage(){

    }

    public ErrorMessage(int scNotFound, String message) {

    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessgae() {
        return messgae;
    }

    public void setMessgae(String messgae) {
        this.messgae = messgae;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "status=" + status +
                ", messgae='" + messgae + '\'' +
                '}';
    }


}
