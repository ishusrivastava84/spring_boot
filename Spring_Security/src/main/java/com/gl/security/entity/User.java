package com.gl.security.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    private Integer userId;
    private String username;
    private String useremail;
    private String useraddress;

    public User(){

    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }

    public User(Integer userId, String username, String useremail, String useraddress) {
        this.userId = userId;
        this.username = username;
        this.useremail = useremail;
        this.useraddress = useraddress;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", useremail='" + useremail + '\'' +
                ", useraddress='" + useraddress + '\'' +
                '}';
    }
}
