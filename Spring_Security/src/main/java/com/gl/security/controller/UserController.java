package com.gl.security.controller;

import com.gl.security.entity.User;
import com.gl.security.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/All")
    public List<User> getAllUser(){
        return this.userService.getAllUser();
    }

    @GetMapping("/name/{username}")
    public User getUserByName(@PathVariable("username") String username){
        return this.userService.getUserByName(username);
    }

    @GetMapping("/id/{id}")
    public User getUserBYId(@PathVariable("id") Integer userId){
        return this.userService.getUserBYId(userId);
    }

    @PostMapping("/Add")
    public User addUser(@RequestBody User user){
        return this.userService.addUser(user);
    }





}
