package com.gl.demo.services;

import com.gl.demo.entity.Student;

public interface StudentService {
    Student createStudent(Student student);
}
