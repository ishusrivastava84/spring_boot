package com.gl.demo.controller;

import com.gl.demo.entity.Student;
import com.gl.demo.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stu")
public class StudentController {

    @Autowired
    private StudentService studentService;

//    @GetMapping("/home")
//    public String home(){
//        return "Hello World";
//    }
    @PostMapping("/crt")
    public Student createStudent(@RequestBody Student student){
        return studentService.createStudent(student);
    }

}
