package com.gl.parking.management.Entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Token {

    private int token_id;

    private LocalDate checkin_date;
    private LocalTime checkin_time;
    private LocalTime checkout_time;

    public Token(){
        //No args cons
    }

    public int getToken_id() {
        return token_id;
    }

    public void setToken_id(int token_id) {
        this.token_id = token_id;
    }

    public LocalTime getCheckin_time() {
        return checkin_time;
    }

    public void setCheckin_time(LocalTime checkin_time) {
        this.checkin_time = LocalTime.from(checkin_time);
    }

    public LocalTime getCheckout_time() {
        return checkout_time;
    }

    public void setCheckout_time(LocalTime checkout_time) {
        this.checkout_time = checkout_time;
    }

    public LocalDate getCheckin_date() {
        return checkin_date;
    }

    public void setCheckin_date(LocalDate checkin_date) {
        this.checkin_date = checkin_date;
    }

    public Token(int token_id, LocalDate checkin_date, LocalTime checkin_time, LocalTime checkout_time) {
        this.token_id = token_id;
        this.checkin_date = checkin_date;
        this.checkin_time = checkin_time;
        this.checkout_time = checkout_time;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token_id=" + token_id +
                ", checkin_date=" + checkin_date +
                ", checkin_time=" + checkin_time +
                ", checkout_time=" + checkout_time +
                '}';
    }
}
