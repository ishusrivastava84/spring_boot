package com.gl.parking.management.Entity;

public class Vehicle {

    private int vehicle_id;
    private String vehicle_name;
    private String owner_name;
    private Token token_id;

    public Vehicle(){
        //No args cons
    }

    public int getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(int vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public Token getToken_id() {
        return token_id;
    }

    public void setToken_id(Token token_id) {
        this.token_id = token_id;
    }

    public Vehicle(int vehicle_id, String vehicle_name, String owner_name, Token token_id) {
        this.vehicle_id = vehicle_id;
        this.vehicle_name = vehicle_name;
        this.owner_name = owner_name;
        this.token_id = token_id;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicle_id=" + vehicle_id +
                ", vehicle_name='" + vehicle_name + '\'' +
                ", owner_name='" + owner_name + '\'' +
                ", token_id=" + token_id +
                '}';
    }
}
