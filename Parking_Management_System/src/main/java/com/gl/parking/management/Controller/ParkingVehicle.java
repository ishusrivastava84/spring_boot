package com.gl.parking.management.Controller;

import com.gl.parking.management.Entity.Token;
import com.gl.parking.management.Entity.Vehicle;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingVehicle {

    List<Token> al=new ArrayList<Token>();
    List<Vehicle> vl=new ArrayList<Vehicle>();

    Token t=new Token();

    Vehicle v=new Vehicle();

    public void AddToken(){
        al.add(new Token(1, LocalDate.now(), LocalTime.now(),LocalTime.now().plusMinutes(30)));
        al.add(new Token(2, LocalDate.now(), LocalTime.now(),LocalTime.now().plusMinutes(30)));
        al.add(new Token(3, LocalDate.now(), LocalTime.now(),LocalTime.now().plusMinutes(30)));
        al.add(new Token(4, LocalDate.now(), LocalTime.now(),LocalTime.now().plusMinutes(30)));
        al.add(new Token(5, LocalDate.now(), LocalTime.now(),LocalTime.now().plusMinutes(30)));
        System.out.println(al);
    }

    public void AddVehicle(){
        vl.add(new Vehicle(1,"car","ishu",new Token(1, LocalDate.now(), LocalTime.now(),LocalTime.now().plusMinutes(30))));
        vl.add(new Vehicle(2,"bike","ashutosh",new Token(2, LocalDate.now(), LocalTime.now().plusHours(1),LocalTime.now().plusMinutes(30))));
        vl.add(new Vehicle(3,"scooty","madhur",new Token(3, LocalDate.now(), LocalTime.now(),LocalTime.now().plusHours(2).plusMinutes(30))));
        vl.add(new Vehicle(4,"bus","rishi",new Token(4, LocalDate.now(), LocalTime.now(),LocalTime.now().plusHours(3).plusMinutes(30))));
        vl.add(new Vehicle(5,"bicycle","mayank",new Token(5, LocalDate.now(), LocalTime.now(),LocalTime.now().plusHours(4).plusMinutes(30))));
        for(Vehicle v1:vl){
            System.out.println(v1);
        }
        vl.stream().forEach(System.out::println);

    }

    List<Vehicle> vehicles=vl.stream().filter(m->m.getVehicle_id()<50).collect(Collectors.toList());

    public void NotEnoughSpace(){
        if(t.getToken_id()<=50){
            System.out.println("Parking space is avilabel");
//            AddVehicle();
        }
        else{
            System.out.println("No space is avilabel");
        }
    }

    public void VehicleCheckout(){
        if(t.getCheckout_time() == LocalTime.of(0,30)){
            System.out.println("Checkout time");
        }
        else if(t.getCheckout_time() == LocalTime.of(0,35)){
            System.out.println("Extra charge is added");
        }
    }
}
