package com.gl.stream.api.Service;

import com.gl.stream.api.Entity.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Scanner;

public class StreamAPI {

    List<Employee> emp=new ArrayList<Employee>();
    Scanner sc=new Scanner(System.in);

    public void addEmployee() {
        emp.add(new Employee(1, "ishu", "Orai", 56000, "IT", "ishu@ishu"));
        emp.add(new Employee(2, "ashutosh", "Orai", 100000, "Civil", "ashu@singh"));
        emp.add(new Employee(3, "madhur", "Sagar", 70000, "Mech", "madhur@madhur"));


        //List of employee those name start with i
        List<Employee> empp = emp.stream().filter(m -> m.getEmpName().startsWith("i")).collect(Collectors.toList());

        //inc the bonus on specif employee
        List<Long> emppp=emp.stream().filter(m->m.getEmpName().startsWith("i")).map(i->i.getEmpSalary()+500).collect(Collectors.toList());
        System.out.println(emppp);

        

    }

}
