package com.gl.banking.system.service;

import com.gl.banking.system.entity.Customer;
import com.gl.banking.system.entity.NewAccount;

import java.util.List;

public interface BankingService {


    NewAccount openNewAccount(NewAccount newAccount);
    
    List<Customer> getAllCustomerList();

    Customer findCustomerById(Long cusNo);
}
