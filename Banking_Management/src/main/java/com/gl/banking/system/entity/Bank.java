package com.gl.banking.system.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "banking")
public class Bank {

    @Id
    @Column(name = "cus_accno")
    private Long cusNo;
    @Column(name="cus_name")
    private String cusName;
    @Column(name="cus_acctype")
    private String accType;
    @Column(name="balance")
    private double balance;
    @Column(name="deposit_balance")
    private double depositbalance;
    @Column(name="credit_balance")
    private double creditbalance;


}
