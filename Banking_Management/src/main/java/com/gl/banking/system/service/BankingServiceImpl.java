package com.gl.banking.system.service;

import com.gl.banking.system.entity.Customer;
import com.gl.banking.system.entity.NewAccount;
import com.gl.banking.system.repository.BankRepo;
import com.gl.banking.system.repository.CustomerRepo;
import com.gl.banking.system.repository.NewAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankingServiceImpl implements BankingService{

    @Autowired
    private NewAccountRepo accountRepo;
    @Autowired
    private CustomerRepo customerRepo;
    @Autowired
    private BankRepo bankRepo;

    @Override
    public NewAccount openNewAccount(NewAccount newAccount) {
        return accountRepo.save(newAccount);
    }

    @Override
    public List<Customer> getAllCustomerList() {
        return null;
    }

    @Override
    public Customer findCustomerById(Long cusNo) {
        return null;
    }
}
