package com.gl.banking.system.repository;

import com.gl.banking.system.entity.NewAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
public interface NewAccountRepo extends JpaRepository<NewAccount,Long> {
}
