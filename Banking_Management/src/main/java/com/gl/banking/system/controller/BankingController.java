package com.gl.banking.system.controller;


import com.gl.banking.system.entity.Customer;
import com.gl.banking.system.entity.NewAccount;
import com.gl.banking.system.service.BankingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bank")
public class BankingController{

    @Autowired
    private BankingService bankservice;

    @PostMapping("/open")
    public NewAccount openNewAccount(@RequestBody NewAccount newAccount){
        return bankservice.openNewAccount(newAccount);
    }

    @GetMapping("/getnewall")
    public List<Customer> getAllCustomerList(){
        return bankservice.getAllCustomerList();
    }

    @GetMapping("/getnewbyid/{id}")
    public Customer findCustomerById(@PathVariable("id") Long cusNo){
        return 44bankservice.findCustomerById(cusNo);
    }



}
