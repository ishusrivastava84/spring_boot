package com.gl.banking.system.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "bank_customer")
public class Customer {
    @Id
    @Column(name = "cus_accno")
    private Long cusNo;
    @Column(name = "cus_name")
    private String cusName;
    @Column(name = "cus_acctype")
    private String accType;
    @Column(name = "balance")
    private double balance;


}
