package com.gl.banking.system;

import com.gl.banking.system.entity.NewAccount;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankingManagementApplication {

	public static void main(String[] args) {

		SpringApplication.run(BankingManagementApplication.class, args);


	}



}
