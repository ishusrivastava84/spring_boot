package com.gl.banking.system.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "new_bank_account")
public class NewAccount {

    @Id
    @Column(name = "cus_accno")
    private Long cusNo;
    @Column(name="acc_type")
    private String accType;
    @Column(name="cus_name")
    private String cusName;
    @Column(name = "balance")
    private double balance;
    @Column(name = "cus_address")
    private String cusAddress;
    @Column(name="cus_identity")
    private String cusIdentityProofNo;
    @Column(name="cus_dob")
    private LocalDate cusDOB;
}
